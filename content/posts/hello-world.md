+++
title = "Hello World"
date = "2020-04-17"
author = "Craig"
description = "My first post"
draft = false
+++

It is tradition to write "Hello World" when embarking on a writing journey, so Hello World! I'm so pleased to make your acquaintance and I hope you enjoy reading my humble website. Please send an email or write on my twitter
